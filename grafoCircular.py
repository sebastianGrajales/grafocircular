import os

class Archivo:
	"""docstring for ClassName"""
	def __init__(self):
		self.__nombreArchivo="grafo.txt"

	def __existeArchivo(self):
		if os.path.isfile(self.__nombreArchivo):
			return True
		else:
			return False

	def grafo(self):
		dic = {}
		if self.__existeArchivo():
			archivo = open(self.__nombreArchivo,'r')
			for linea in archivo:
				nuevaLinea = linea.rstrip()
				nodoDireccion = nuevaLinea.split(',',1)
				dic.setdefault(nodoDireccion[0],nodoDireccion[1])
			archivo.close()
		return dic

class Grafo(object):
	"""docstring for grafo"""
	def __init__(self):
		self.__archivo = Archivo()
		self.__grafo = self.__archivo.grafo()

	def esCircular(self):
		grafo = self.__grafo.items()
		listanodo = []
		listanodo.append(list(self.__grafo)[0])
		ubicacionNodo = list(self.__grafo)[0]
		for nodo, destino in grafo:
			if(nodo==ubicacionNodo):
				listanodo.append(destino)
				ubicacionNodo=destino
			else:
				return "No es un grafo Circular"
		if(listanodo[0]==listanodo[len(listanodo)-1]):
			return "Es un grafo Circular"
		else:
			return "No es un grafo Circular"

grafo = Grafo()
print(grafo.esCircular())
		
